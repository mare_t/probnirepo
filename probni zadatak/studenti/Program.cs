﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace studenti
{
    class Program
    {
        static void Main(string[] args)
        {
            List<student> studenti = new List<student>();
            student ucenik = new student();
            studenti = ucenik.getstudenti();
            var upit = (from lik in studenti //linq
                              where lik.starost < 30 && lik.ocene.Average() >= 5
                              orderby lik.ocene.Average() descending
                              select lik).Take(3);
            Console.WriteLine("Ovako bi izgledala promena koda + dodatak");
            Console.WriteLine("Promena broj 2 + dodatak");
            using (StreamWriter sw = new StreamWriter("studenti.txt"))
            {
                foreach (student s in upit)
                {
                    Console.WriteLine(s.ime + " " + s.prezime);
                    sw.WriteLine(s.ime + " " + s.prezime);
                }
            }
            Console.WriteLine("Vaskov dodatak");
        }
    }
}
